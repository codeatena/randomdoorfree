//
//  Common.m
//  mobitalent
//
//  Created by wang chenglei on 12/3/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "Common.h"
#import <AVFoundation/AVFoundation.h>

@implementation Common

+ (float)getScreenHeight {
    return [[UIScreen mainScreen] bounds].size.height;
}

+ (BOOL)is_Retina {
    return ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0) ;
}

+ (BOOL)is_Phone {
    
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) ? TRUE : FALSE;
}

+ (int)getDeviceType {
    
    if ([Common is_Phone]) {
        // iPhone
        
        if ([Common is_Retina]) {
            // if iPhone retina
            
            if ([Common getScreenHeight] > 480) {
                return DEVICE_TYPE_IPHONE_RETINA_4_INCH;
            }
            else {
                return DEVICE_TYPE_IPHONE_RETINA_35_INCH;
            }
        }
        else {
            // if iPhone
            return DEVICE_TYPE_IPHONE;
        }
    }
    else {
        // iPad
        
        if ([Common is_Retina]) {
            return DEVICE_TYPE_IPAD_RETINA;
        }
        else {
            return DEVICE_TYPE_IPAD;
        }
    }
    
    return -1;
}

+ (NSString*)getXibNameWithSuffix:(NSString*)xibName {

    NSString *strNibName = xibName;
    
    if ([Common is_Phone]) {
        strNibName = [NSString stringWithFormat:@"%@_iPhone", strNibName];
    }
    else {
        strNibName = [NSString stringWithFormat:@"%@_iPad", strNibName];
    }
    
    if ([Common getDeviceType] == DEVICE_TYPE_IPHONE_RETINA_4_INCH) {
        strNibName = [NSString stringWithFormat:@"%@_Retina_4", strNibName];
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        strNibName = [NSString stringWithFormat:@"%@_V7", strNibName];
    }
    
    return strNibName;
}

+ (NSString*)getXibNameWithSuffixWithoutVersion:(NSString*)xibName {
    
    NSString *strNibName = xibName;
    
    if ([Common is_Phone]) {
        strNibName = [NSString stringWithFormat:@"%@_iPhone", strNibName];
    }
    else {
        strNibName = [NSString stringWithFormat:@"%@_iPad", strNibName];
    }
    
    if ([Common getDeviceType] == DEVICE_TYPE_IPHONE_RETINA_4_INCH) {
        strNibName = [NSString stringWithFormat:@"%@_Retina_4", strNibName];
    }
    
    return strNibName;
}

+ (NSString *)getCellXibNameWithSuffix:(NSString *)xibName {
    
    if ([Common is_Phone]) {
        
        return [NSString stringWithFormat:@"%@_iPhone", xibName];
    }
    else {
        
        return [NSString stringWithFormat:@"%@_iPad", xibName];
    }
    
    return @"";
}

+ (UIImage *)resizeImage:(UIImage *)image newSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContext(newSize);
    
    CGRect newRect = CGRectMake(0, 0, newSize.width, newSize.height);
    [image drawInRect:newRect];
    
    UIImage *targetImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return targetImage;
}

+ (NSString*)getStringFromDiffDate:(double)time standardTime:(double)standardTime {

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDate *dateNow = [NSDate dateWithTimeIntervalSince1970:standardTime];
    
    NSTimeInterval diff_time = [dateNow timeIntervalSinceDate:date];
    
    float year = 365 * 24 * 60 * 60;
    float month = 30 * 24 * 60 * 60;
    float day = 24 * 60 * 60;
    float hour = 60 * 60;
    float minute = 60;
    
    int nDiff = (int)(diff_time / year);
    if (nDiff > 0) {
        return [NSString stringWithFormat:@"%dy", nDiff];
    }

    nDiff = (int)(diff_time / month);
    if (nDiff > 0) {
        return [NSString stringWithFormat:@"%dmo", nDiff];
    }
    
    nDiff = (int)(diff_time / day);
    if (nDiff > 0) {
        return [NSString stringWithFormat:@"%dd", nDiff];
    }
    
    nDiff = (int)(diff_time / hour);
    if (nDiff > 0) {
        return [NSString stringWithFormat:@"%dh", nDiff];
    }
    
    nDiff = (int)(diff_time / minute);
    if (nDiff > 0) {
        return [NSString stringWithFormat:@"%dm", nDiff];
    }
    
    nDiff = (int)(diff_time);
    if (nDiff > 0) {
        return [NSString stringWithFormat:@"%ds", nDiff];
    }

    return @"0";
}

+ (NSData *)makeThumbFromFilePath:(NSString *)videoPath {
    
    NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    generator.appliesPreferredTrackTransform = TRUE;
    generator.maximumSize = CGSizeMake(280, 210);
    
    CGImageRef thumbImageRef = [generator copyCGImageAtTime:CMTimeMake(1 * 60, 60) actualTime:nil error:nil];
    UIImage *thumbImage = [UIImage imageWithCGImage:thumbImageRef];
    CGImageRelease(thumbImageRef);
    
    NSData *pngData = UIImagePNGRepresentation(thumbImage);
    return pngData;
}

+ (NSData *)makeThumbFromFilePathWithRange:(NSString *)videoPath startTime:(double)startTime duration:(double)duration {
    
    NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    generator.appliesPreferredTrackTransform = TRUE;
    generator.maximumSize = CGSizeMake(280, 210);
    
    CGImageRef thumbImageRef = [generator copyCGImageAtTime:CMTimeMake(startTime, duration) actualTime:nil error:nil];
    UIImage *thumbImage = [UIImage imageWithCGImage:thumbImageRef];
    CGImageRelease(thumbImageRef);
    
    NSData *pngData = UIImagePNGRepresentation(thumbImage);
    return pngData;
}

+ (NSData *)makeThumbFromURL:(NSString *)videoURL {
    
    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:[NSURL URLWithString:videoURL] options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    generator.appliesPreferredTrackTransform = TRUE;
    generator.maximumSize = CGSizeMake(320, 180);

    CGImageRef thumbImageRef = [generator copyCGImageAtTime:CMTimeMake(1 * 60, 60) actualTime:nil error:nil];
    UIImage *thumbImage = [UIImage imageWithCGImage:thumbImageRef];
    CGImageRelease(thumbImageRef);
    
    NSData *pngData = UIImagePNGRepresentation(thumbImage);
    return pngData;
}

+ (NSString *)makeIntNumber:(int)number {

    if (number < pow(10, 3)) {
        return [NSString stringWithFormat:@"%d", number];
    }

    if (pow(10, 3) <= number && number < pow(10, 6)){
        return [NSString stringWithFormat:@"%.1fK", (float)number/pow(10, 3)];
    }
    
    if (pow(10, 6) <= number && number < pow(10, 9)){
        return [NSString stringWithFormat:@"%.1fM", (float)number/pow(10, 6)];
    }

    return @"0";
}

+ (NSString *)makeIntNumberUsingThousandUnit:(int)number {
    
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    numberFormat.usesGroupingSeparator = YES;
    numberFormat.groupingSeparator = @",";
    numberFormat.groupingSize = 3;
    
    NSString* str = [numberFormat stringFromNumber:[NSNumber numberWithInt:number]];
    return str;
}

+ (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    UIRectCorner corner; //holds the corner
    
    //Determine which corner(s) should be changed
    if (tl) {
        corner = UIRectCornerTopLeft;
    }
    if (tr) {
        corner = UIRectCornerTopRight;
    }
    if (bl) {
        corner = UIRectCornerBottomLeft;
    }
    if (br) {
        corner = UIRectCornerBottomRight;
    }
    if (tl && tr) { //top
        corner = UIRectCornerTopRight | UIRectCornerTopLeft;
    }
    if (bl && br) { //bottom
        corner = UIRectCornerBottomLeft | UIRectCornerBottomRight;
    }
    if (tl && bl) { //left
        corner = UIRectCornerTopLeft | UIRectCornerBottomLeft;
    }
    if (tr && br) { //right
        corner = UIRectCornerTopRight | UIRectCornerBottomRight;
    }
    
    if (tl & tr & bl & br) {
        corner = UIRectCornerAllCorners;
    }
    
    UIView *roundedView = view;
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = roundedView.bounds;
    maskLayer.path = maskPath.CGPath;
    roundedView.layer.mask = maskLayer;
    
    return roundedView;
}

@end
