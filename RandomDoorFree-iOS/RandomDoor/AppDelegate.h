//
//  AppDelegate.h
//  RandomDoor
//
//  Created by JinSung Han on 2/28/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)showWaitingScreen:(NSString *)strText bShowText:(BOOL)bShowText;
- (void)hideWaitingScreen;

@end

AppDelegate* g_delegate;
