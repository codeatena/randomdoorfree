//
//  ShareViewController.m
//  RandomDoor
//
//  Created by JinSung Han on 5/31/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ShareViewController.h"
#import "FullViewController.h"
#import <Social/Social.h>

#define SHARE_DIALOG_AD_BANNER_UNIT_ID         @"ca-app-pub-5533489369819223/9642874793"

@interface ShareViewController ()

@end

@implementation ShareViewController

@synthesize parentViewCtrl, bmpFullImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    bannerView_ = [[GADBannerView alloc]
                   initWithFrame:CGRectMake(20, 330, 280, 50)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = SHARE_DIALOG_AD_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    //    bannerView_.backgroundColor = [UIColor redColor];
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    [bannerView_ loadRequest:[GADRequest request]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(id)sender
{
    [self.view removeFromSuperview];
    FullViewController* fullViewCtrl = (FullViewController*) parentViewCtrl;
    
    [fullViewCtrl hide];
}

- (IBAction)onFacebook:(id)sender
{
    [self.view removeFromSuperview];

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *faceSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [faceSheet addImage: bmpFullImage];
        [parentViewCtrl presentViewController:faceSheet animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information!" message:@"Your phone doesnt have the facebook app installed. Download the facebook app to share random-door on facebook." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

- (IBAction)onTwitter:(id)sender
{
    [self.view removeFromSuperview];

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet addImage:bmpFullImage];
        
        [parentViewCtrl presentViewController:tweetSheet animated:YES completion:nil];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Information!" message:@"Your phone doesnt have the twitter app installed. Download the Twitter app to share random-door on twitter." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

- (IBAction)onInstagram:(id)sender
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image.igo"];
        
        NSData *data = UIImagePNGRepresentation(bmpFullImage);
        [data writeToFile:savedImagePath atomically:YES];
        
        NSURL *imageUrl = [NSURL fileURLWithPath:savedImagePath];
        
        if (docController == NULL) {
            docController = [[UIDocumentInteractionController alloc] init];
            docController.delegate = self;
        }
        
        docController.UTI = @"com.instagram.photo";
        docController.URL = imageUrl;
        [docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
    }
    else
    {
        UIAlertView *errorToShare = [[UIAlertView alloc] initWithTitle:@"Information!" message:@"Your phone doesnt have the instagram app installed. Download the instagram app to share random-door on instagram." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorToShare show];
    }
    
    [self.view removeFromSuperview];
}

- (IBAction)onCancel:(id)sender
{
    [self.view removeFromSuperview];
}

@end
